const { MongoClient } = require('mongodb');
const callItc = require('./modules/itc');

const connectWithRetry = async () => new Promise((resolve, reject) => {
  MongoClient.connect('mongodb://mongo:27017', { useNewUrlParser: true }, (err, client) => {
    if (err) {
      console.log(err);
      setTimeout(connectWithRetry, 5000);
    } else {
      resolve(client.db('backend').collection('subscriptions'));
    }
  });
});

// взято с модуля subscriptions
const decodeStatus = (uid, itcDataEncoded, receipt) => {
  const itcData = JSON.parse(itcDataEncoded);

  if (itcData.environment === 'Sandbox') {
    return null;
  }

  const status = {
    uid,
    auto_renew_status: (itcData.auto_renew_status || itcData.pending_renewal_info[0].auto_renew_status) === '1',
    in_retry: (itcData.is_in_billing_retry_period || itcData.pending_renewal_info[0].is_in_billing_retry_period) === '1',
    receipt,
  };

  if (itcData.expiration_intent) {
    let intent = itcData.expiration_intent;
    if (intent === '1') intent = 'cancel';
    if (intent === '2') intent = 'billing';
    if (intent === '4') intent = 'unavail';
    if (intent === '5') intent = 'unknown';

    status.expiration_intent = intent;
  }

  if (itcData.latest_expired_receipt_info) {
    // у юзера просрочено продление подписки
    const lastExpRec = itcData.latest_expired_receipt_info;
    status.valid_until = parseInt(lastExpRec.expires_date_ms);
    status.is_trial = false;
    status.original_purchase_date = parseInt(lastExpRec.original_purchase_date_ms);
  }

  if (itcData.latest_receipt_info) {
    // у юзера активна подписка, возможно триал
    let lastRec = null;
    if (Array.isArray(itcData.latest_receipt_info)) {
      const sortedData = itcData.latest_receipt_info.sort((a, b) => 
        parseInt(b.purchase_date_ms) - parseInt(a.purchase_date_ms));
      lastRec = sortedData[0];
    } else {
      lastRec = itcData.latest_receipt_info;
    }
    status.valid_until = parseInt(lastRec.expires_date_ms);
    status.is_trial = lastRec.is_trial_period === 'true';
    status.original_purchase_date = parseInt(lastRec.original_purchase_date_ms);
  } else if (itcData.receipt) {
    // самый свежий рецепт
    const lastRec = itcData.receipt;
    status.valid_until = parseInt(lastRec.expires_date_ms);
    status.is_trial = lastRec.is_trial_period === 'true';
    status.original_purchase_date = parseInt(lastRec.original_purchase_date_ms);
  }

  status.has_access = new Date().getTime() < status.valid_until;

  return status;
};

const solve = async () => {
  const db = await connectWithRetry();

  // возьмем все неактивные подписки
  const inactive = await db.find({ auto_renew_status: false }).sort({ updated_at: -1 }).toArray();

  let updatedCount = 0;
  // нужно заново запросить у apple инфу по рецептам
  const inactiveWithItc = await Promise.all(inactive.map(async (sub) => {
    try {
      sub.itc = await callItc(sub.receipt);
      sub.status = decodeStatus(sub.uid, sub.itc, sub.receipt);

      if (sub.status && !sub.status.auto_renew_status) {
        const itcData = JSON.parse(sub.itc);
        await db.updateOne({ uid: sub.uid }, { $set: { unsubscribed_at: +itcData.receipt.receipt_creation_date_ms } });
        console.log(`Set unsubscribed_at ${itcData.receipt.receipt_creation_date_ms} to ${sub.uid}`);
      }

      if (sub.status && sub.auto_renew_status !== sub.status.auto_renew_status) {
        await db.updateOne({ uid: sub.uid }, { $set: { auto_renew_status: sub.status.auto_renew_status } });
        updatedCount += 1;
      }

      return sub;
    } catch (err) {
      console.log(sub.uid + ': ' + err);
      console.log(sub);
    }
  }));

  console.log('Complete!');
  console.log('Count of updated subscriptions:' + updatedCount);
};

solve();
