// Методы чтобы пробрасывать эвенты о подписках/отписках/ребиллах в трекер Бином

const request = require('request');
const logger = require('./log.js');
const config = require('./config.js');

const types = {
  REBILL: 'rebill',
  SUBSCRIBED: 'subscribe',
  UNSUBSCRIBED: 'stop',
};

// прост метод которым можно слать постебки в трекер-бином
// обновлять статусы подписок
function postback(uid, status, payout) {
  // possible status: subscribe, billed, stop
  // пример вызова: binom.com/click.php?cnv_id=51fa1blb27&payout=3&cnv_status=charge_more

  if (uid.includes('binom')) {
    const binomId = uid.replace('binom_', '');
    const url = `http://${
      config.binomDomain
    }/click.php?cnv_id=${binomId}&payout=${payout}&cnv_status=${status}`;
    request(url, (err) => {
      if (err) {
        logger.error(`Binom postback: ${err} - ${url}`);
      } else {
        logger.info(`Binom postback: ${uid} ${status} ${payout}`);
      }
    });
  }
}

module.exports.post = postback;
module.exports.t = types;
