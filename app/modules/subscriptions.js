const { MongoClient } = require('mongodb');
const logger = require('./log.js');
const config = require('./config.js');
const callItc = require('./itc.js');
const events = require('./events.js');
const binom = require('./binom');

let db;

// самый ебучий кусок кода тут, который типа отслеживает статусы подписок
// трекает ребиллы, отписки и тд; работает с артефактами, если у вас прямые руки,
// то лучше тут все переписать

const connectWithRetry = () => {
  MongoClient.connect('mongodb://mongo:27017', { useNewUrlParser: true }, (err, client) => {
    if (err) {
      setTimeout(connectWithRetry, 5000);
    } else {
      db = client.db('backend').collection('subscriptions');
    }
  });
};

connectWithRetry();

/*
структура подписки в базе данных:
    uid
    receipt
    auto_renew_status
    valid_until
    is_trial
    original_purchase_date
    expiration_intent
    has_access
    revenue
    updated_at
    unsubscribed_at
*/

const decodeStatus = (uid, itcDataEncoded, receipt) => {
  const itcData = JSON.parse(itcDataEncoded);

  const status = {
    uid,
    auto_renew_status: (itcData.auto_renew_status || itcData.pending_renewal_info[0].auto_renew_status) === '1',
    in_retry: (itcData.is_in_billing_retry_period || itcData.pending_renewal_info[0].is_in_billing_retry_period) === '1',
    receipt,
  };
  
  if (itcData.expiration_intent) {
    let intent = itcData.expiration_intent;
    if (intent === '1') intent = 'cancel';
    if (intent === '2') intent = 'billing';
    if (intent === '4') intent = 'unavail';
    if (intent === '5') intent = 'unknown';

    status.expiration_intent = intent;
  }

  if (itcData.latest_expired_receipt_info) {
    // у юзера просрочено продление подписки
    const lastExpRec = itcData.latest_expired_receipt_info;
    status.valid_until = parseInt(lastExpRec.expires_date_ms);
    status.is_trial = false;
    status.original_purchase_date = parseInt(lastExpRec.original_purchase_date_ms);
    status.plan = lastExpRec.product_id;
  }

  if (itcData.latest_receipt_info) {
    // у юзера активна подписка, возможно триал
    let lastRec = null;
    if (Array.isArray(itcData.latest_receipt_info)) {
      const sortedData = itcData.latest_receipt_info.sort((a, b) => 
        parseInt(b.purchase_date_ms) - parseInt(a.purchase_date_ms));
      lastRec = sortedData[0];
    } else {
      lastRec = itcData.latest_receipt_info;
    }
    status.valid_until = parseInt(lastRec.expires_date_ms);
    status.is_trial = lastRec.is_trial_period === 'true';
    status.original_purchase_date = parseInt(lastRec.original_purchase_date_ms);
    status.plan = lastRec.product_id;
  } else if (itcData.receipt) {
    // самый свежий рецепт
    const lastRec = itcData.receipt;
    status.valid_until = parseInt(lastRec.expires_date_ms);
    status.is_trial = lastRec.is_trial_period === 'true';
    status.original_purchase_date = parseInt(lastRec.original_purchase_date_ms);
    status.plan = lastRec.product_id;
  }

  status.has_access = new Date().getTime() < status.valid_until;

  return status;
};

const mergeStatus = (old, current) => {
  const merged = Object.assign({}, current);
  const plan = current.plan;
  const currentRevenue = config && config.plans && plan && config.plans[plan] ? config.plans[plan] : 0;

  if (old && current.valid_until > old.valid_until) {
    // юзер продлил старую подписку
    merged.revenue = old.revenue + currentRevenue;
    logger.info(`Merge status: ${current.uid} – rebill`);
  }

  if (old && current.valid_until <= old.valid_until) {
    // юзер восстановил старую подписку через restore_purchase
    merged.revenue = old.revenue;
  }

  if (!old && current.is_trial) {
    // юзер впервые оформил новую подписку с триалом
    merged.revenue = 0;
    logger.info(`Merge status: ${current.uid} – new subscription (trial)`);
  }

  if (old && old.auto_renew_status && !current.auto_renew_status) {
    merged.unsubscribed_at = new Date().getTime();
    logger.info(`Merge status: ${current.uid} – unsubscribed`);
  }

  if (!old && !current.is_trial) {
    // старый рецепт, с нового uid; restore_purchase после удаления аппы?
    merged.revenue = 0;
    logger.info(`Merge status: ${current.uid} – old receipt, new uid`);

    if (current.valid_until > new Date().getTime()) {
      // было продление
      merged.revenue = currentRevenue;
      logger.info(`Merge status: ${current.uid} – rebill for lost original uid`);
    } else {
      // юзер восстановил протухшую через restore_purchase
      merged.revenue = 0;
      logger.info(`Merge status: ${current.uid} – restore expired sub for lost original uid`);
    }
  }

  return merged;
};

const triggerEvents = (old, merged) => {
  const geo = (old && old.geo) || (merged && merged.geo) || null;
  if (!old && merged.is_trial) {
    // new subscription
    binom.post(merged.uid, binom.t.SUBSCRIBED, 0);
    events.push(merged.uid, events.t.TRIAL_START, geo);
  }
  if (old && old.revenue < merged.revenue) {
    // rebill
    binom.post(merged.uid, binom.t.REBILL, merged.revenue);
    events.push(merged.uid, events.t.REBILL, geo);
  }
  if (old && old.auto_renew_status && !merged.auto_renew_status) {
    // unsubscribed
    binom.post(merged.uid, binom.t.UNSUBSCRIBED, 0);
    events.push(merged.uid, events.t.UNSUB, geo);
  }
  if (
    old &&
    old.auto_renew_status === merged.auto_renew_status &&
    old.valid_until === merged.valid_until
  ) {
    // nothing changed
    logger.info(`Merge status: ${merged.uid} – nothing changed`);
  }
};

const getOldStatus = async uid => db.findOne({ uid });

const saveSubscription = (sub, id = null) => {
  const ss = Object.assign({}, sub);
  ss.updated_at = new Date().getTime();

  db.updateOne({ uid: ss.uid, ...(!!id && { _id: id }) }, { $set: ss }, { upsert: true });
};

const handleReceipt = async (uid, receipt, geo) => {
  logger.info(`HANDLE_RECEIPT: \n${receipt}\n\n`);
  const itcData = await callItc(receipt);
  logger.debug(`ITCDATA: \n${JSON.stringify(itcData, null, 2)}\n\n`);
  const currentStatus = decodeStatus(uid, itcData, receipt);
  logger.debug(`CURRENT_STATUS: \n${JSON.stringify(currentStatus, null, 2)}\n\n`);
  const oldStatus = await getOldStatus(uid);
  logger.debug(`OLD_STATUS: \n${JSON.stringify(oldStatus, null, 2)}\n\n`);
  const mergedStatus = mergeStatus(oldStatus, currentStatus);
  logger.debug(`MERGED: \n${JSON.stringify(mergedStatus, null, 2)}\n\n`);

  if (geo) {
    mergedStatus.geo = geo.toLowerCase();
  }


  triggerEvents(oldStatus, mergedStatus);
  saveSubscription(mergedStatus);

  // DEBUG SECTION
  // delete currentStatus.receipt;
  // delete oldStatus.receipt;
  // delete mergedStatus.receipt;
  // delete itcData.receipt;
  // log('\n\n');
  // log(`ITCDATA: ${JSON.stringify(JSON.parse(itcData), null, 4)}`);
  // log(`OLD: ${JSON.stringify(oldStatus, null, 4)}`);
  // log(`CURRENT: ${JSON.stringify(currentStatus, null, 4)}`);
  // log(`MERGED: ${JSON.stringify(mergedStatus, null, 4)}`);
  // END ON DEBUG

  return mergedStatus;
};

const refreshActive = async (res) => {
  const now = new Date().getTime();
  const rate = now - 43200000; // 12 hours converted to ms

  const awaiting = await db
    .find({ auto_renew_status: true, valid_until: { $lt: now }, updated_at: { $lt: rate } })
    .count();

  const subs = await db
    .find({ auto_renew_status: true, valid_until: { $lt: now }, updated_at: { $lt: rate } })
    .sort({ updated_at: 1 })
    .limit(10)
    .toArray();

  if (res) {
    res.send({
      STATUS: 'UPDATING',
      NOW: subs.length,
      QUEUE: awaiting,
    });
  }

  if (subs.length > 0) {
    logger.info(`Updating Subs: ${subs.length} / ${awaiting}`);
  }

  subs.forEach(async (sub) => {
    const oldStatus = sub;
    const itcData = await callItc(sub.receipt);
    const currentStatus = decodeStatus(sub.uid, itcData, sub.receipt);
    const mergedStatus = mergeStatus(oldStatus, currentStatus);

    triggerEvents(oldStatus, mergedStatus);

    saveSubscription(mergedStatus, sub._id);
  });
};

const getStatus = async (uid) => {
  const sub = await getOldStatus(uid);

  if (sub) {
    logger.info(`Get Status: ${uid} access: ${sub.has_access}`);
    return {
      valid_until: sub.valid_until,
    };
  }
  logger.info(`Get Status: ${uid} no sub found`);
  return {
    valid_until: 0,
  };
};

module.exports.handleReceipt = handleReceipt;
module.exports.refreshActive = refreshActive;
module.exports.getStatus = getStatus;

module.exports.one = uid => getOldStatus(uid);

module.exports.active = async () => db.find({ auto_renew_status: true }).toArray();

module.exports.inactive = async () => db.find({ auto_renew_status: false }).toArray();

module.exports.all = async () => db.find().toArray();

module.exports.revenue = async () =>
  db
    .aggregate([
      {
        $group: {
          _id: null,
          total: { $sum: '$revenue' },
        },
      },
    ])
    .toArray();
