const { MongoClient } = require('mongodb');

// простенький модуль, который сохраняет приходящие эвенты от приложенй в монгу

/*
формат эвента в базе:
    datetime (unixtime ms?)
    uid
    event (string)
*/

let db;

const connectWithRetry = () => {
  MongoClient.connect('mongodb://mongo:27017', { useNewUrlParser: true }, (err, client) => {
    if (err) {
      setTimeout(connectWithRetry, 5000);
    } else {
      db = client.db('backend').collection('events');
    }
  });
};

connectWithRetry();

const types = {
  INSTALL: 'APP_INSTALL',
  TRIAL_START: 'TRIAL_START',
  TRIAL_END: 'TRIAL_END',
  REBILL: 'REBILL',
  UNSUB: 'UNSUBSCRIBE',
  CHURN: 'CHURN',
  CONNECT: 'CONNECT',
  REVIEWTAP: 'TAP_REVIEW',
  TIMEOUT: 'TIMEOUT',
  SCREENSHOT: 'SCREENSHOT',
};
const untrackedTypes = [types.TRIAL_START, types.REBILL, types.UNSUB];

const newEvent = (uid, event, geo) => {
  const ev = {
    datetime: new Date(),
    uid,
    event,
    ...(geo && { geo })
  };

  db.insertOne(ev).catch((err) => {
    console.log(err);
  });

  // need to update all events without geo and set this geo
  if (geo) {
    db.update(
      { uid, geo: { $exists: false } },
      { $set: { geo: geo.toLowerCase() } },
      { multi: true },
    );
  }
};

async function listEvents(uid) {
  if (uid) {
    return db
      .find({ uid, event: { $not: /SHUTDOWN_VPN_\w+_\w+/ } })
      .sort({ datetime: -1 })
      .limit(50)
      .toArray();
  }

  return db
    .find({ event: { $not: /SHUTDOWN_VPN_\w+_\w+/ } })
    .sort({ datetime: -1 })
    .limit(50)
    .toArray();
}

async function listSubscriptions(uid) {
  const subEventKeys = [types.REBILL, types.TRIAL_START, types.TRIAL_END, types.UNSUB];

  if (uid) {
    return db
      .find({ uid, event: { $in: subEventKeys } })
      .sort({ datetime: -1 })
      .toArray();
  }

  return db
    .find({ event: { $in: subEventKeys } })
    .sort({ datetime: -1 })
    .limit(50)
    .toArray();
}

async function getLastEvent(event, args = {}) {
  return db.findOne({ event, ...args }, { sort: { datetime: -1 } });
}

async function getCount(event, args = {}) {
  return db.find({ event, ...args }).count();
}

module.exports.push = newEvent;
module.exports.list = listEvents;
module.exports.subs = listSubscriptions;
module.exports.t = types;
module.exports.untrackedTypes = untrackedTypes;
module.exports.getLast = getLastEvent;
module.exports.getCount = getCount;
