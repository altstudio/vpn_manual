const moment = require('moment-timezone');
const { createLogger, format, transports } = require('winston');

// просто запись и форматирование логов
const timestamp = () =>
  moment(new Date())
    .tz('Europe/Moscow')
    .format('D MMM, HH:mm:ss');

const fmt = format.printf(info => `${timestamp()} – ${info.message}`);

const options = {
  file: {
    level: 'debug',
    filename: '/home/app/logs/app.log',
    handleExceptions: true,
    maxsize: 5242880, // 5MB
    maxFiles: 10,
  },
  console: {
    level: 'info',
    handleExceptions: true,
  },
};

const logger = createLogger({
  format: fmt,
  transports: [new transports.File(options.file), new transports.Console(options.console)],
  exitOnError: true,
});

module.exports = logger;
