/* ignore jslint */
const { MongoClient } = require('mongodb');

const events = require('./events');
const _ = require('lodash');

/*
EVENT STRUCTURE:
  datetime
  uid
  event //trial_start, trial_end, rebill, unsubscribed

  geo
*/

let db;
let subDB;

const connectWithRetry = () => {
  MongoClient.connect('mongodb://mongo:27017', { useNewUrlParser: true }, (err, client) => {
    if (err) {
      console.log(err);
      setTimeout(connectWithRetry, 5000);
    } else {
      db = client.db('backend').collection('events');
      subDB = client.db('backend').collection('subscriptions');
    }
  });
};

connectWithRetry();

const types = events.t; // types of events

const cStart = new Date(0);
const cEnd = new Date();

const getSubs = async () => db
  .find({
    event: types.TRIAL_START,
    $and: [
      { datetime: { $gte: cStart } },
      { datetime: { $lte: cEnd } },
    ],
  })
  .toArray();

const getRebills = async (subAgeList, geo = null) => db
  .find({
    event: types.REBILL,
    uid: {
      $in: subAgeList,
    },
    ...(geo && { geo }),
  })
  .toArray();

const getSubscriptions = async uids => subDB
  .find({
    uid: {
      $in: uids,
    },
    geo: { $exists: true },
  }, {
    geo: 1,
  }).toArray();

const getLTV = async () => {
  const rebillPrice = 7;

  const daysBetween = (start, end) => {
    const oneDay = 24 * 60 * 60 * 1000;
    const firstDate = new Date(start);
    const secondDate = new Date(end);
    return Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / oneDay));
  };

  let allSubs = _(await getSubs());
  const uids = [...new Set(...allSubs.map(s => s.uid))];
  const subscriptions = await getSubscriptions(uids);
  allSubs = allSubs.map((sub) => {
    const geoSub = subscriptions.find(s => s.uid);

    return { ...sub, ...(!!geoSub && !sub.geo && { geo: geoSub.geo }) };
  });

  const allGeos = allSubs.filter(sub => !!sub.geo).map(sub => sub.geo.toLowerCase()).value();
  const geos = ['summary', ...new Set(allGeos)];

  const getLTVForGeo = async (geo = 'summary') => {
    const subs = geo === 'summary' ? allSubs : allSubs.filter(s => s.geo && s.geo.toLowerCase() === geo.toLowerCase());
    if (geo !== 'summary') {
      console.log(subs.value());
    }
    const subStart = Object.assign(
      {},
      ...subs.map(e => ({ [e.uid]: new Date(e.datetime) })).value(),
    );

    const subAgeList = Object.assign(
      {},
      ...subs.map(e => ({ [e.uid]: daysBetween(e.datetime, new Date()) })).value(),
    );

    const subAges = subs.map(e => daysBetween(e.datetime, new Date())).value();

    const countSubs = minAge => _.filter(subAges, s => s >= minAge).length;

    const rebills = _(await getRebills(Object.keys(subAgeList), null)).map(e => ({
      onDay: daysBetween(e.datetime, subStart[e.uid]),
      subAge: subAgeList[e.uid],
    })).value();

    const countRebills = uptoDay =>
      _.filter(rebills, s => s.onDay <= uptoDay && s.subAge >= uptoDay).length;

    const values = [];

    for (let day = 0; day < 180; day += 1) {
      const subsCount = countSubs(day);
      const rebillsCount = countRebills(day);
      const daySpend = (rebillsCount * rebillPrice) / subsCount;

      if (rebillsCount > 0) {
        values.push([day, subsCount, daySpend.toFixed(2)]);
      }
    }

    return { geo, values };
  };

  return Promise.all(geos.map(getLTVForGeo));
};

module.exports.getLTV = getLTV;
