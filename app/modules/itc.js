const request = require('request');
const logger = require('./log.js');
const config = require('./config.js');

// функция дергает itunes с рецептом полученным от приложений
// сначала дергает продакшн хост, если от него приходит ошибка 21007, значит это рецепт из песочницы
// и тогда дергает песочницу
// вернувшиеся данные потом используются в subscriptions.js
function loadItcData(receipt) {
  return new Promise((resolve, reject) => {
    const productionHost = 'buy.itunes.apple.com';
    const sandboxHost = 'sandbox.itunes.apple.com';
    const payload = {
      'receipt-data': receipt,
      password: config.itcPassword,
    };
    request.post(
      {
        url: `https://${productionHost}/verifyReceipt`,
        body: JSON.stringify(payload),
        headers: {
          'Content-Type': 'application/json',
        },
      },
      (err, res, body) => {
        if (err) {
          logger.error(`Load ITC data: request error - ${err}`);
          reject(err);
          return;
        }

        if (JSON.parse(body).status === 21007) {
          logger.info('Test receipt was sent to production endpoint 21007');
          request.post(
            {
              url: `https://${sandboxHost}/verifyReceipt`,
              body: JSON.stringify(payload),
              headers: {
                'Content-Type': 'application/json',
              },
            },
            (err2, res2, body2) => {
              if (err) {
                logger.error(`Load ITC data 2: request error - ${err}`);
                reject(err);
                return;
              }
              resolve(body2);
            },
          );
        } else {
          resolve(body);
        }
      },
    );
  });
}

module.exports = loadItcData;
