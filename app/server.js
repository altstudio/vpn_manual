const express = require('express');
const favicon = require('serve-favicon');
const useragent = require('express-useragent');
const moment = require('moment-timezone');
const { table } = require('table');
const morgan = require('morgan');
const auth = require('http-auth');
const telegram = require('telegram-bot-api');

const cloaker = require('./modules/cloaker');
const config = require('./modules/config');
const events = require('./modules/events');
const logger = require('./modules/log');
const subscriptions = require('./modules/subscriptions');
const ltv = require('./modules/ltv');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';
const fd = ts =>
  moment(new Date(ts))
    .tz('Europe/Moscow')
    .format('  D MMM, HH:mm:ss  ');

// App
const app = express();
app.use(favicon(`${__dirname}/../www/images/favicon.ico`));
app.use(express.json());
app.use(useragent.express());
app.enable('trust proxy');

const basic = auth.basic({
  realm: config.realm
}, function(username, password, callback) {
  callback(password === config.authPassword);
});
const authMiddleware = auth.connect(basic);

const api = new telegram({
    token: config.botToken,
});

// Morgan, настройка логирования; все логи – в папке logs рядом с кодом
const morganf = 'Morgan: :method :url :status - :response-time ms';
app.use(morgan(morganf, {
  stream: { write: message => logger.info(message.trim()) },
}));

// обработка кликов с лендинга, даём клоакеру запомнить ip и редиректим в стор
// либо не редиректим, если сервер в режиме "только белые установки"
app.get('/click', (req, res) => {
  cloaker.click(req);
  if (!config.isAllWhite) {
    res.redirect(config.appLink);
  } else {
    logger.info('Cloaker: ALLWHITE – ALLWHITE – ALLWHITE');
    res.sendStatus(200);
  }
});

// регистрация при первом запуске мобильного приложения
app.get('/rgs', cloaker.reg);

// когда юзер подписывается, прила присылает сюда рецепт
// рецепт валидируется и в ответ уходит timestamp до которого у юзера оплачен доступ
app.post('/purchase', async (req, res) => {
  const { uid, receipt, geo } = req.body;
  const status = await subscriptions.handleReceipt(uid, receipt, geo);
  res.json({
    valid_until: status.valid_until,
  });
});

// насильное обновление статуса подписок, когда хочется посмотреть на статку
// почти не использовался в последнее время
app.get('/refresh', (req, res) => {
  subscriptions.refreshActive(res);
});

// обрабатывает запросы о статусе подписок
app.get('/check', async (req, res) => {
  res.json(await subscriptions.getStatus(req.query.uid));
});

// просмотр одной конкретной подписки, в виде как она лежит в монге
app.get('/one', async (req, res) => {
  res.json(await subscriptions.one(req.query.uid));
});

const shutdownResendTime = 1000*60*10;

// трекинг эвентов, они просто сохраняются в монгу и пишутся в лог
// схавает любой эвент, так что можете в приложениях наставить самых разных, какие хотите измерять
app.get('/track', async (req, res) => {
  const { uid, event, user_geo, vpn_geo } = req.query;

  let ev = event;
  if (ev === 'SHUTDOWN_VPN') {
    const regexSended = new RegExp(`${ev}_${vpn_geo}_\\w+_SENDED`);
    const regex = new RegExp(`${ev}_${vpn_geo}_\\w+`);

    ev = `${ev}_${vpn_geo}_${user_geo}`;

    const lastShutdownVPN = await events.getLast(regexSended, { 
        datetime: { $gte: new Date(+new Date - shutdownResendTime*2)}
      });
    if (!lastShutdownVPN || (lastShutdownVPN && +new Date(lastShutdownVPN.datetime) + shutdownResendTime < +new Date)) {
      const count = lastShutdownVPN ? (await events.getCount(regex, { 
        datetime: { $gte: lastShutdownVPN.datetime}
      })) : 1;
      api.sendMessage({
        chat_id: config.chatId,
        text: 
`Message: VPN shutdown
Server: ${config.servername}
VPN geo: ${vpn_geo}
User geo: ${user_geo}
Count: ${count}`,
        parse_mode: 'Markdown'
      });

      ev = `${ev}_SENDED`;
    }
  }

  // logger.info(`TRACK_CALL: uid: ${uid} event: ${event}`);
  if (!events.untrackedTypes.includes(ev.toUpperCase())) {
    events.push(uid, ev.toUpperCase());
  }

  res.json({
    ok: true,
  });
});

// статистика по подпискам
app.get('/money', async (req, res) => {
  const evs = await events.subs(req.query.uid);

  const evTable = [['DATETIME', 'UID', 'EVENT']];
  evs.forEach((ev) => {
    evTable.push([fd(ev.datetime), ev.uid, ev.event]);
  });
  res.set('Content-Type', 'text/plain');
  res.send(`MONEY EVENTS
    \n${table(evTable)}
  `);
});

// последнние эвенты из базы
app.get('/ev', authMiddleware, async (req, res) => {
  const evs = await events.list(req.query.uid);
  const evTable = [['DATETIME', 'UID', 'EVENT']];
  evs.forEach((ev) => {
    evTable.push([fd(ev.datetime), ev.uid, ev.event]);
  });
  res.set('Content-Type', 'text/plain');
  res.send(`ALL EVENTS
    \n${table(evTable)}
  `);
});

const rebillOverdue = (ts) => {
  const now = new Date();
  if (now > ts) {
    return `Overdue ${((now - ts) / 1000 / 3600 / 24).toFixed(1)} days`;
  }
  return fd(ts);
};

// ltv table
app.get('/ltv', authMiddleware, async (req, res) => {
  const ltvData = await ltv.getLTV();

  const tableHeaders = ['DAY', 'SUBSCRIPTIONS', 'DAYSPEND ($)'];
  const tables = ltvData.map(result => ({
    title: result.geo,
    values: [tableHeaders, ...(result.values.length ? result.values : [])],
  }));

  const report = tables.map(t => `
    ${t.title.toUpperCase()}
    \n${table(t.values)}
  `).join('\n\n');

  res.set('Content-Type', 'text/plain');
  res.send(report);
});

// таблица всех подписок
app.get('/pp', authMiddleware, async (req, res) => {
  const activeSubs = await subscriptions.active();
  const inactiveSubs = await subscriptions.inactive();
  const totalSubs = await subscriptions.all();
  let revenue = await subscriptions.revenue();
  revenue = revenue[0].total;

  const activeTable = [
    ['UID', 'NEXT REBILL', 'SUBSCRIBED', 'REVENUE', 'TRIAL', 'INTENT', 'IN RETRY', 'UPDATED'],
  ];
  activeSubs.forEach((sub) => {
    activeTable.push([
      sub.uid,
      rebillOverdue(sub.valid_until), // overdue
      fd(sub.original_purchase_date),
      sub.revenue,
      sub.is_trial,
      sub.expiration_intent,
      sub.in_retry,
      fd(sub.updated_at),
    ]);
  });

  const inactiveTable = [['UID', 'UNSUBSCRIBED', 'SUBSCRIBED', 'REVENUE', 'INTENT', 'UPDATED']];
  inactiveSubs.forEach((sub) => {
    inactiveTable.push([
      sub.uid,
      fd(sub.unsubscribed_at),
      fd(sub.original_purchase_date),
      sub.revenue,
      sub.expiration_intent,
      fd(sub.updated_at),
    ]);
  });

  const report = `
    SUBSCRIPTIONS
    Active: ${activeSubs.length}
    inactive: ${inactiveSubs.length}
    Total: ${totalSubs.length}
    Revenue: ${revenue}
    

    ACTIVE SUBSCRIPTIONS:
    \n${table(activeTable)}

    INACTIVE SUBSCRIPTIONS:
    \n${table(inactiveTable)}
  `;

  res.set('Content-Type', 'text/plain');
  res.send(report);
});

// так нода отдаёт статический сайт из папки
// путь такой странный потому что там в docker-compose сюда папка сайта монтируется
app.use(express.static('../www'));

app.listen(PORT, HOST);
logger.info(`ServerStart: Running on http://${HOST}:${PORT}`);

// SUBSCRIPTION REFRESH
// на одном из ядер запускается скрипт, который трекает статус подписки
if (process.env && process.env.pm_id) {
  if (Number(process.env.pm_id) === 1) {
    const interval = 1000 * 10; // every 10sec
    logger.info('SubRefresher will start soon');
    setInterval(() => {
      subscriptions.refreshActive();
    }, interval);
  }
}
